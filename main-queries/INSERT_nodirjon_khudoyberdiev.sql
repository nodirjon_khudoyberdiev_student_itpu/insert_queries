

-- Choose one of your favorite films and add it to the "film" table. Fill in rental rates with 4.99 and rental durations with 2 weeks.
INSERT INTO film (title, description, release_year, language_id, rental_duration, rental_rate, length, replacement_cost, rating, last_update, special_features)
VALUES (
    '22 Mile', 
    'Description of the film "22 Mile".', 
    2018,  -- Assuming the film was released in 2018
    (SELECT language_id FROM language WHERE name = 'English'), 
    14, 
    4.99, 
    120,  -- Assuming the length of the film is 120 minutes
    19.99, 
    'R',   -- Assuming the film has an 'R' rating
    '2024-04-05 10:00:00',  -- Replace with a specific timestamp value,
    '{"Trailers", "Behind the Scenes"}' -- Enclose special features in curly braces to represent an array
);

--Add the actors who play leading roles in your favorite film to the "actor" and "film_actor" tables (three or more actors in total
WITH actors AS (
  SELECT actor_id
  FROM actor
  WHERE (first_name, last_name) IN (('Mark', 'Wahlberg'), ('Lauren', 'Cohan'),('Nick','Vallelonga'))
)
INSERT INTO film_actor (actor_id, film_id, last_update)
SELECT actor_id, (SELECT film_id FROM film WHERE title = '22 Mile'), current_timestamp
FROM actors;



--Add your favorite movies to any store's inventory.
INSERT INTO inventory (film_id, store_id, last_update)
VALUES ((SELECT film_id FROM film WHERE title = '22 Mile'), 1, current_timestamp);
